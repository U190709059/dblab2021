select * from shippers; /*prints all the infos inside the shippers*/

/*to combine two entity use join or in => subquery method */
select shipperid, count(orderid)
from orders
where shipperid in(
	select shipperid 
    from shippers
	/*no need for when clause because we do not need any specific shipper company*/
)
group by shipperid;

select orders.shipperid, count(orderid)/*orders.shipperid can be changed to shippername*/
from orders join shippers on orders.shipperid=shippers.shipperid
group by orders.shipperid;
    
/*number of orders that our shipped by company*/

/*all the customer names that have ordered at least once-join customer and order than find the wanted attribute*/
select customername, orderid
from orders join customers on /*based on shared attribute comes now*/ customers.customerid=orders.customerid
order by orderid; /*inner join example*/

/*all the customer name-left join example*/
select customername, orderid  
from customers left join orders on customers.customerid=orders.customerid
order by orderid;

/*getting full name- right join example*/
select firstname, lastname, orderid
from orders right join employees on orders.employeeid=employees.employeeid
order by orderid;

/*union and distinct can be used to mimic full outer join*/