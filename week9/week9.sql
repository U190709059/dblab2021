select * from customers;

/*changing a row is done by update 
first we will update rows with the errors*/

update customers set country=replace(country, "\n", '');/*changes empty spaces in the data*/
update customers set city=replace(city, "\n", '');

create view mexicanCustomers as
select customerid,customername,contactname
from customers
where country="Mexico";

select * from mexicancustomers;

select *
from mexicancustomers join orders on mexicancustomers.customerid=orders.customerid;

create view productbelowavg as
select productid, productname,price
from products
where price < (select avg(price) from products);

select * from productbelowavg;

delete from orderdetails;/*deletes all rows from orderdetails*/
select * from orderdetails;/*shows rows from orderdetails*/

delete from orderdetails where ProductID=5;/*product id=5 rows are deleted-removes spesific line use this one*/
truncate orderdetails;/*delete from and truncate works similar way difference is only in performance they both deletes rows from selected table*/

delete from customers;/*statement gives error because customers is paired with other table, assigned connections must be severed, customer id is used as a foreign key in order table*/
delete from orders;/*gives same error as above, orderid is used by orderdetails*/

drop table customers;/*drop table "tablename" drop whole thing from server*/ 
/*ddl data definition language command*/
/*data;definition,manipulation,retriveal*/

