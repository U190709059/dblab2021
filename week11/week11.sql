call getcustomersbycountry("Mexico");

set @country = "UK";
select @country;
call getcustomersbycountry(@country);
select @country;

select * from Shippers;

select count(OrderID) 
from Orders join Shippers on Orders.ShipperID=Shippers.ShipperID
where ShipperName="Speedy Express";

select @inc;

set @OrderCount=0;
call getnumberofordersbyshipper("Speedy Express", @ordercount);
select @ordercount;

set @beg = 100;
set @inc = 10;

call counter(@beg, @inc);
select @beg;
select @inc;

select * from movies;
select * from denormalized;

load data 
infile '/var/lib/mysql-files/' #no permission on lab computer
#infile "/home/student/Downloads/denormalized.csv"
into table denormalized
columns terminated by ";";

show variables like "secure_file_priv";

insert into movies(movie_id, title, ranking, rating, year, votes, duration, oscars, budget)
select distinctrow movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

select * from movies;

insert into countries(country_id, country_name)
select distinctrow producer_country_id, producer_country_name
from denormalized
union
select distinctrow director_country_id, director_country_name
from denormalized
union
select distinctrow star_country_id, star_country_name
from denormalized
order by producer_country_id;

select * from countries;