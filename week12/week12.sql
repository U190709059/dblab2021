select * from proteins;

explain select * from proteins where pid like "5HT2C_HUMA%";
select * from proteins where pid like "5HT2C_HUMA%";

explain select * from proteins where accession="Q9UBA6";
select * from proteins where accession="Q9UBA6";

create unique index idx1 using btree on proteins(pid);
alter table proteins add constraint acc_pk primary key (accession);
#accession part is column name, adds constrains to table
#after "using" one of the hash on or btree on data structures is used 
#hash cannot be used on innodb engine and if we use it instead using hash system uses btree and creates index

alter table proteins drop index idx1;
alter table proteins drop primary key;
#if data has many floats, integers,varchars try to use hash type!!!
#constrains manipulations is better done before data insertion!
#with indexes and primary keys insertion we decrease the row number and speed up the procedure
#to see results first delete indexes and primary keys and execute, searches this gives us long result
#after executing index creation commands execute searchers, this gives us short result

